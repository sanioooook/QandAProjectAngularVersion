
export class Vote {

  public dateVote: string;

  public id: number;

  public idAnswer: number;

  public voter: string;

  public isUserVote: boolean;
}
